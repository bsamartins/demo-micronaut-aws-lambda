package io.bsamartins.demo.micronaut.aws.lambda

import io.micronaut.core.annotation.Introspected

@Introspected
class HelloRequest {
    lateinit var name: String
}
