package io.bsamartins.demo.micronaut.aws.lambda

import io.micronaut.function.FunctionBean
import io.micronaut.function.aws.MicronautRequestHandler
import javax.inject.Inject
import javax.inject.Singleton

@FunctionBean("hello-function")
class HelloFunction: java.util.function.Function<HelloRequest, HelloResponse> {
    override fun apply(request: HelloRequest): HelloResponse {
        println("Request: $request")
        return HelloResponse(message = "Hello ${request.name}")
    }
}

@Singleton
class HelloFunctionHandler: MicronautRequestHandler<HelloRequest, HelloResponse>() {

    @Inject
    lateinit var function: HelloFunction

    override fun execute(input: HelloRequest): HelloResponse {
        return function.apply(input)
    }
}

