package io.bsamartins.demo.micronaut.aws.lambda

import io.micronaut.core.annotation.Introspected

@Introspected
data class HelloResponse(val message: String)
