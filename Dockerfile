FROM quay.io/quarkus/ubi-quarkus-native-image:20.0.0-java11

ADD build/libs/demo-micronaut-aws-lambda-all.jar /app/

RUN native-image --no-server \
	-H:Class=io.micronaut.function.aws.runtime.MicronautLambdaRuntime \
	--verbose \
	-cp /app/demo-micronaut-aws-lambda-all.jar
