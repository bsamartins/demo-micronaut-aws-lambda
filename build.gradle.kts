plugins {
    kotlin("jvm") version "1.3.72"
    kotlin("kapt") version "1.3.72"
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom:1.3.72"))
    implementation(platform("io.micronaut:micronaut-bom:1.3.4"))
    kapt(platform("io.micronaut:micronaut-bom:1.3.4"))

    kapt("io.micronaut:micronaut-inject-java")
    kapt("io.micronaut:micronaut-validation")
    kapt("io.micronaut:micronaut-graal")

    implementation(kotlin("stdlib"))

    implementation("io.micronaut:micronaut-function-aws")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    compileOnly("org.graalvm.nativeimage:svm")
    implementation("io.micronaut.aws:micronaut-function-aws-custom-runtime:1.4.1")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    mergeServiceFiles()
}
